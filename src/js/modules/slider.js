console.log('slider')

var swiper = new Swiper(".technology-icons", {
    loop: true,
    effect: 'flip', 
    spaceBetween: 0,
    autoplay: {
        delay: 1000,
    },
});

var swiper = new Swiper(".reviews__slider", {
    loop: true,
    effect: 'flip', 
    direction: "vertical",
    spaceBetween: 1,
    slidesPerView: 1,
    autoplay: {
        delay: 3000,
    },
});